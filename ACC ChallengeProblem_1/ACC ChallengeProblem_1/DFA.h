#pragma once

#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<windows.h>

#define T 50
#define verde 10
#define albastru 11
#define rosu 12
#define galben 14
#define alb 15

struct DFA_transition {
	std::string state_1;
	char symbol;
	std::string state_2;
};

class DFA
{
//private:
public:
	std::vector<std::string> Q;
	std::string Sigma;
	std::vector<DFA_transition> Delta;
	std::string q0;
	std::vector<std::string> F;

public:
	void readDFA(std::string fileName);
	void printDFA();
};

