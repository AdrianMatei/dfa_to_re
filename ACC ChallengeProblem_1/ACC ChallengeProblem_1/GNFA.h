#pragma once

#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<windows.h>

#include"DFA.h";

#define T 50
#define verde 10
#define albastru 11
#define rosu 12
#define galben 14
#define alb 15

struct GNFA_transition {
	std::string state_1;
	std::string regularExpression;
	std::string state_2;
};

class GNFA
{
private:
	std::vector<std::string> Q;
	std::string Sigma;
	std::vector<GNFA_transition> Delta;
	std::string q_start;
	std::string q_accept;

public:
	void buildGNFA(DFA M);
	void printGNFA();
	void eliminateTrapStates();
	void simplifyTransitions();
	std::string convert(GNFA G);

	std::string findR(std::string state_1, std::string state_2);
	bool validState(std::string state_1, std::string state_2);
};

