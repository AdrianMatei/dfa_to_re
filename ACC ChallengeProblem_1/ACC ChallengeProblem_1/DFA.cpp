#include "DFA.h"

void DFA::readDFA(std::string fileName)
{
	std::ifstream fin(fileName);

	std::string states;
	std::getline(fin, states);
	for (int index = 0; index < states.length(); index++)
	{
		std::string state;
		while (index < states.length() && states[index] != ' ' && states[index] != '\n')
		{
			state.push_back(states[index]);
			index++;
		}
		Q.push_back(state);
		state.clear();
	}

	std::string alphabet;
	std::getline(fin, alphabet);
	for (int index = 0; index < alphabet.length(); index++)
		if (alphabet[index] != ' ')
			Sigma.push_back(alphabet[index]);

	for (int index = 0; index < Q.size() * Sigma.size(); index++)
	{
		DFA_transition t;
		fin >> t.state_1 >> t.symbol >> t.state_2;
		Delta.push_back(t);
	}

	std::getline(fin, q0);
	std::getline(fin, q0);

	std::getline(fin, states);
	for (int index = 0; index <= states.length(); index++)
	{
		std::string state;
		while (index < states.length() && states[index] != ' ' && states[index] != '\0')
		{
			state.push_back(states[index]);
			index++;
		}
		F.push_back(state);
		state.erase();
	}
}

void DFA::printDFA()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	system("chcp 1253");
	system("CLS");

	SetConsoleTextAttribute(hConsole, galben);
	std::cout << "\n DFA:\n";

	SetConsoleTextAttribute(hConsole, albastru);
	std::cout << "\n Q = {";
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << Q[index];
		if (index != Q.size() - 1)
			std::cout << ", ";
	}
	std::cout << "}\n\n ";
	Sleep(T);
	std::cout << (char)211 << " = {";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << ", ";
	}
	std::cout << '}';
	Sleep(T);
	std::cout << "\n\n " << (char)228 << ": " << Delta[0].state_1 << " -(" << Delta[0].symbol << ")-> " << Delta[0].state_2;
	Sleep(T);
	for (int index = 1; index < Delta.size(); index++)
	{
		std::cout << "\n    " << Delta[index].state_1 << " -(" << Delta[index].symbol << ")-> " << Delta[index].state_2;
		Sleep(T);
	}
	std::cout << "\n\n q0 = " << q0;
	Sleep(T);
	std::cout << "\n\n F = {";
	for (int index = 0; index < F.size(); index++)
	{
		std::cout << F[index];
		if (index != F.size() - 1)
			std::cout << ", ";
	}
	std::cout << "}\n\n";
	Sleep(T);
	SetConsoleTextAttribute(hConsole, alb);
	//system("pause");
}
