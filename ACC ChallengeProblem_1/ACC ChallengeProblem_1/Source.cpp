#include<iostream>
#include<fstream>

#include"DFA.h";
#include"GNFA.h";

int main()
{
	DFA M;

	M.readDFA("DFA_5.txt");
	//M.readDFA("ChallangeProblem2Answer.txt");
	M.printDFA();
	system("pause");

	GNFA MPrime;

	MPrime.buildGNFA(M);
	MPrime.printGNFA();
	MPrime.eliminateTrapStates();
	MPrime.printGNFA();
	MPrime.simplifyTransitions();
	MPrime.printGNFA();
	std::string regularExpression = MPrime.convert(MPrime);
	
	std::cout << "\n\n Regular expression: " << regularExpression << "\n\n\n";
	//system("pause");
	return 0;
}