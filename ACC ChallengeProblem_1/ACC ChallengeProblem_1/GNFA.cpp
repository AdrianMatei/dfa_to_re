#include <queue>

#include "GNFA.h"

void GNFA::buildGNFA(DFA M)
{
	this->Q = M.Q;
	this->Q.push_back("s");
	this->Q.push_back("a");

	this->q_start = "s";
	this->q_accept = "a";

	this->Sigma = M.Sigma;

	//this->Delta = M.Delta;
	for (int index = 0; index < M.Delta.size(); index++)
	{
		GNFA_transition t;
		t.state_1 = M.Delta[index].state_1;
		t.regularExpression = M.Delta[index].symbol;
		t.state_2 = M.Delta[index].state_2;
		Delta.push_back(t);
	}
	GNFA_transition t;
	t.state_1 = "s";
	t.regularExpression = "~";
	t.state_2 = M.q0;
	Delta.push_back(t);
	for (int index = 0; index < M.F.size(); index++)
	{
		t.state_1 = M.F[index];
		t.regularExpression = "~";
		t.state_2 = "a";
		Delta.push_back(t);
	}
}

void GNFA::printGNFA()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	system("chcp 1253");
	system("CLS");

	SetConsoleTextAttribute(hConsole, galben);
	std::cout << "\n GNFA:\n";

	SetConsoleTextAttribute(hConsole, albastru);
	std::cout << "\n Q = {";
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << Q[index];
		if (index != Q.size() - 1)
			std::cout << ", ";
	}
	std::cout << "}\n\n ";
	Sleep(T);
	std::cout << (char)211 << " = {";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << ", ";
	}
	std::cout << '}';
	Sleep(T);
	if(Delta.size() > 0)
		std::cout << "\n\n " << (char)228 << ": " << Delta[0].state_1 << " -(" << Delta[0].regularExpression << ")-> " << Delta[0].state_2;
	Sleep(T);
	for (int index = 1; index < Delta.size(); index++)
	{
		std::cout << "\n    " << Delta[index].state_1 << " -(" << Delta[index].regularExpression << ")-> " << Delta[index].state_2;
		Sleep(T);
	}
	std::cout << "\n\n q_start = " << q_start;
	Sleep(T);
	std::cout << "\n\n q_accept = " << q_accept;
	std::cout << "\n\n";
	Sleep(T);
	SetConsoleTextAttribute(hConsole, alb);
	
	//system("pause");
	Sleep(250);
}

void GNFA::eliminateTrapStates()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	int initialSize = Q.size();
	std::vector<std::string> trapStates;

	std::queue<std::string> BF;
	std::vector<std::string> visitedStates;

	for (int state = 0; state < Q.size() - 2; state++)
	{
		while (BF.size() > 0)
			BF.pop();

		visitedStates.clear();

		BF.push(Q[state]);
		visitedStates.push_back(Q[state]);

		do {

			std::string currentState = BF.front();

			BF.pop();

			for (int index = 0; index < Delta.size(); index++)
				if (Delta[index].state_1 == currentState)
				{
					bool OK = 1;
					for (int visitedState = 0; visitedState < visitedStates.size(); visitedState++)
						if (visitedStates[visitedState] == Delta[index].state_2)
							OK = 0;

					if (OK == 1)
					{
						BF.push(Delta[index].state_2);
						visitedStates.push_back(Delta[index].state_2);
					}
				}

		} while (BF.size() > 0 && BF.front() != "a");

		if (BF.size() == 0)
		{
			for (int index = 0; index < Delta.size(); index++)
				if (Delta[index].state_1 == Q[state] || Delta[index].state_2 == Q[state])
				{
					Delta.erase(Delta.begin() + index);
					index--;
				}

			trapStates.push_back(Q[state]);
			Q.erase(Q.begin() + state);

			state--;
		}
	}

	if (initialSize != Q.size())
	{
		SetConsoleTextAttribute(hConsole, galben);
		std::cout << "\n Eliminating trap states...";
		for (int index = 0; index < trapStates.size(); index++)
			std::cout << "\n    Eliminated state " << trapStates[index];
		std::cout << "\n\n\n\n";
	}
	else
	{
		SetConsoleTextAttribute(hConsole, galben);
		std::cout << "\n No trap states found.\n\n\n\n";
	}
	SetConsoleTextAttribute(hConsole, alb);
	system("pause");
}

void GNFA::simplifyTransitions()
{
	for (int transition_1 = 0; transition_1 < Delta.size(); transition_1++)
	{
		for (int transition_2 = transition_1 + 1; transition_2 < Delta.size(); transition_2++)
		{
			if (Delta[transition_1].state_1 == Delta[transition_2].state_1 &&
				Delta[transition_1].state_2 == Delta[transition_2].state_2)
			{
				GNFA_transition t;
				t.state_1 = Delta[transition_1].state_1;
				std::string ER = Delta[transition_1].regularExpression;
				ER = ER + "+";
				ER = ER + Delta[transition_2].regularExpression;
				t.regularExpression = ER;
				t.state_2 = Delta[transition_1].state_2;

				Delta[transition_1] = t;

				Delta.erase(Delta.begin() + transition_2);

				transition_1 = 0;
				transition_2 = 0;
			}
		}
	}
}

std::string GNFA::convert(GNFA G)
{
	std::ofstream fout;
	fout.open("transitions.txt", std::ios_base::app);

	int k = G.Q.size();

	if (k == 2)
		return G.Delta[0].regularExpression;

	if (k > 2)
	{
		std::string q_rip = G.Q[0];
		G.Q.erase(G.Q.begin());

		std::vector<GNFA_transition> newTransitions;

		for (int state_1 = 0; state_1 < G.Q.size(); state_1++)
		{
			for (int state_2 = 0; state_2 < G.Q.size(); state_2++)
			{
				if (G.Q[state_1] != "a" && G.Q[state_2] != "s" && (G.validState(G.Q[state_1], q_rip) == 1 && G.validState(q_rip, G.Q[state_2]) == 1))
				{
					std::string qi = G.Q[state_1];
					std::string qj = G.Q[state_2];

					if (qi == "2" && qj == "2")
						std::cout << "";

					std::string R1 = G.findR(qi, q_rip);
					std::string R2 = G.findR(q_rip, q_rip);
					std::string R3 = G.findR(q_rip, qj);
					std::string R4 = G.findR(qi, qj);

					for (int index = 0; index < G.Delta.size(); index++)
						if (G.Delta[index].state_1 == qi && G.Delta[index].state_2 == qj)
						{
							G.Delta.erase(G.Delta.begin() + index);
							index--;
						}

					GNFA_transition newTransition;

					newTransition.state_1 = qi;
					newTransition.state_2 = qj;

					bool parentheses;

					if (R1.size() > 0)
					{
						parentheses = 0;
						for (int index = 0; index < R1.size(); index++)
							if (R1[index] == '+')
								parentheses = 1;

						if (parentheses == 1)
							newTransition.regularExpression += "(" + R1 + ")";
						else
							newTransition.regularExpression += R1;
					}

					if (R2.size() > 0)
					{
						if (R2.size() > 1)
							newTransition.regularExpression += "(" + R2 + ")*";
						else
							newTransition.regularExpression += R2 + "*";
					}

					if (R3.size() > 0)
					{
						parentheses = 0;
						for (int index = 0; index < R3.size(); index++)
							if (R3[index] == '+')
								parentheses = 1;

						if (parentheses == 1)
							newTransition.regularExpression += "(" + R3 + ")";
						else
							newTransition.regularExpression += R3;
					}

					if (R4.size() > 0)
					{
						if ((R1.size() > 0 || R2.size() > 0 || R3.size() > 0) && R4.size() > 0)
							newTransition.regularExpression += "+";

						parentheses = 0;
						for (int index = 0; index < R4.size(); index++)
							if (R4[index] == '+')
								parentheses = 1;

						if (parentheses == 1)
							newTransition.regularExpression += "(" + R4 + ")";
						else
							newTransition.regularExpression += R4;
					}

					for (int index = 0; index < newTransition.regularExpression.size(); index++)
					{
						if (newTransition.regularExpression[index] == '~')
						{
							if ((index > 0 && newTransition.regularExpression[index - 1] != '+') &&
								(index < newTransition.regularExpression.size() - 1 && newTransition.regularExpression[index + 1] != '+'))
							{
								newTransition.regularExpression.erase(newTransition.regularExpression.begin() + index);
								index--;
							}
						}
					}


					if (newTransition.regularExpression.size() > 0)
						newTransitions.push_back(newTransition);
				}
			}
		}
		if (newTransitions.size() > 0)
		{
			//G.Delta.clear();

			for (int index = 0; index < G.Delta.size(); index++)
				if (G.Delta[index].state_1 == q_rip || G.Delta[index].state_2 == q_rip)
				{
					G.Delta.erase(G.Delta.begin() + index);
					index--;
				}

			for (int index = 0; index < newTransitions.size(); index++)
				G.Delta.push_back(newTransitions[index]);

			newTransitions.clear();
		}
		//G.simplifyTransitions();
		G.printGNFA();

		for (int index = 0; index < G.Q.size(); index++)
			fout << G.Q[index] << " ";
		fout << "\n\n";

		for (int index = 0; index < G.Delta.size(); index++)
			fout << G.Delta[index].state_1 << " -[" << G.Delta[index].regularExpression << "]-> " << G.Delta[index].state_2 << "\n";
		fout << "\n\n\n";

		return convert(G);
	}
}

std::string GNFA::findR(std::string state_1, std::string state_2)
{
	std::string RE;
	for (int index = 0; index < Delta.size(); index++)
	{
		if (Delta[index].state_1 == state_1 && Delta[index].state_2 == state_2)
		{
			if (!(Delta[index].regularExpression[0] == '~' && Delta[index].regularExpression.size() == 1))
				RE = RE + Delta[index].regularExpression;
			return RE;
		}
	}

	return "";
}

bool GNFA::validState(std::string state_1, std::string state_2)
{
	for (int index = 0; index < Delta.size(); index++)
		if (Delta[index].state_1 == state_1 && Delta[index].state_2 == state_2)
			return 1;

	return 0;
}
